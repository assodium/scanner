"""
For DWK
    22.8.18 01:42
"""
print('For DWK')

import os
from re import sub
import subprocess

import pytube

yt = pytube.YouTube("https://www.youtube.com/watch?v=3nxbVGl-yzE")

vids= yt.streams.all()

for i in range(len(vids)):
    print(i,'. ',vids[i])

vnum = int(input("Select Num : "))

parent_dir = "A:/"
vids[vnum].download(parent_dir) 
print('Download Done')

new_filename = input("MP3 Name : ")
default_filename = vids[vnum].default_filename 
modified_filename = default_filename.replace(' ', '_')
print('Def Filename : ', parent_dir+modified_filename, parent_dir+new_filename)
subprocess.call(['A:/Apps/ffmpeg/bin/ffmpeg.exe', #'-i', 'A:/a.mp4', 'A:/a.mp3'
    parent_dir+modified_filename,  
    parent_dir+new_filename             
    # os.path.join(parent_dir, modified_filename),
    # os.path.join(parent_dir, new_filename)
])

# subprocess.call('dir {}'.format('/w'), shell=True)
print('Convert Done')