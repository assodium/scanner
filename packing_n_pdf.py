from PIL import Image
import glob
import os

name = ''
pdf_path = name+".pdf"
scan_path = 'c:/github/scanner/scan_out/'
page_list = glob.glob(scan_path + '*.png')

del page_list[-1] 
del page_list[-1]
del page_list[-1]

img_1 = Image.open(scan_path + r'001.png')
page_w, page_h = img_1.size
print(page_w, page_h)
print(page_list)

images = [
    Image.open(f)
    for f in page_list
]
 
images[0].save(
    pdf_path, save_all=True, append_images=images[1:]
)

print('Done : ', pdf_path)


