import re
from timeit import repeat
import pyautogui
import time
from PIL import Image
from PIL import ImageChops
import os
import pydirectinput as pi
from playsound import playsound     
import glob

YES_MODE = False
if not YES_MODE:
    book_area = [150, 95, 923-150, 1000-95]
    btn_pos = [980,551]
else :
    book_area = [40, 80, 917-40, 1000-80]
    btn_pos = [931,569] 

# book_area = [160, 106, 900-160, 1000-106]
# btn_pos = [962,546]
# book_area = [163, 100, 904-163, 1032-100]
# book_area = [40, 80, 917-40, 1000-80]
# btn_pos = [980,551] 
# btn_pos = [931,569] 

png_dir = './scan_out/'

TESTMODE = True
TESTMODE = False

page_list = glob.glob(png_dir + '*.png')

for file in page_list:
    os.remove(file)

for i in range(5):
    print('Ready to Scan.. {}'.format(i))
    time.sleep(1)

repeat_cnt = 0
page_cur = 1 #def. 1 
while True:
    pyautogui.screenshot(png_dir+'{:03}.png'.format(page_cur), region=(book_area[0],book_area[1],book_area[2],book_area[3]))
    print('{:03} scanned'.format(page_cur))
    if not YES_MODE:
        pyautogui.click(btn_pos[0], btn_pos[1], clicks = 1, interval = 1)
        # pi.click(btn_pos[0], btn_pos[1])
        time.sleep(0.5)
    else :
        # pi.press('DOWN')
        #     pi.keyUp('RIGHT')
        pyautogui.scroll(-200, btn_pos[0], btn_pos[1]) 
        # pyautogui.scroll(-200, int((book_area[2]-book_area[0])/2), btn_pos[1]) 
        time.sleep(2)

    if TESTMODE and page_cur > 3:
        print('Try Out')
        # for i in range(3):
        #     playsound('chime.wav') 
        break

    if page_cur > 100:
        image_cur = Image.open(png_dir+'{:03}.png'.format(page_cur)).convert('RGB')
        image_prev = Image.open(png_dir+'{:03}.png'.format(page_cur-1)).convert('RGB')
        diff = ImageChops.difference(image_cur, image_prev)
        if diff.getbbox():     
            repeat_cnt = 0   
            print("On Scan..")
        else:
            if repeat_cnt >= 1:
                print("Scan is ended")
                # for i in range(3):
                #     playsound('chime.wav')
                break
            else:
                print('Repeate_cnt : ', repeat_cnt)
                repeat_cnt += 1
    
    page_cur += 1