from timeit import repeat
import pyautogui
import time
import datetime as dt
from playsound import playsound  
from random import randint   
from manager import messenger
messenger = messenger()

view_area = [234, 649, 935, 907]
btn_search_area = [871, 456, 935-871, 484-456]
btn_search = [920,470]
btn_buy = [529,706]

png_dir = './scan_out/'
page_cur = 1 #def. 1 
alarm_cnt = 15
ref_img = pyautogui.screenshot(png_dir + 'ref_search_btn_img.png', region=(btn_search_area[0],btn_search_area[1],btn_search_area[2],btn_search_area[3]))

print(ref_img)
while True:
    cur_time = dt.datetime.now()
    CUR_TIME = '{:02}{:02}_{:02}{:02}{:02}'.format(cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second)

    pyautogui.click(btn_search[0], btn_search[1], clicks = 1, interval = 1)
    time.sleep(4)
    pyautogui.click(btn_buy[0], btn_buy[1], clicks = 1, interval = 1)

    img = pyautogui.screenshot(region=(btn_search_area[0],btn_search_area[1],btn_search_area[2],btn_search_area[3]))
    if img != ref_img:
        print(CUR_TIME, 'Ticket Available')
        img.save(png_dir + CUR_TIME + '.png')
        for i in range(alarm_cnt):
            msg = '{} : Lets Buy Ticket'.format(CUR_TIME)
            messenger.send_slack_msg('#house_watcher', msg)
            playsound('chime.wav') 
            time.sleep(10)
        break

    else:
        print(CUR_TIME, 'N/A')
        dly_time = randint(10, 30)
        # msg = '{} : Wait More'.format(CUR_TIME)
        # messenger.send_slack_msg('#house_watcher', msg)
        # playsound('chime.wav') 
        time.sleep(dly_time)